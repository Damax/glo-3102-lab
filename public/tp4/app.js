import { getTodo, getUser, createTodo, updateTodo, deleteTodo } from './api.js'

const INPUT_ERROR_CLASS = 'input--error'

const addButton = document.querySelector('#add-button')
const todo = document.querySelector('#todo')

const main = async () => {
    await getUser()
    addButton.addEventListener('click', handleAddClick)
    todo.addEventListener('keydown', (ev) => {
        if (ev.key === 'Enter') {
            handleAddClick(ev)
        }
    })
    await renderTodos()
}

const strToDom = (str) => document.createRange().createContextualFragment(str).firstChild

const handleUpdateClick = async (ev) => {
    const todoId = ev.target.dataset['id']
    if (todoId) {
        const todoInput = document.querySelector(`#todo-${todoId}`)
        if (todoInput.value) {
            todoInput.classList.remove(INPUT_ERROR_CLASS)
            await updateTodo(todoId, todoInput.value)
            await renderTodos()
        } else {
            todoInput.classList.add(INPUT_ERROR_CLASS)
        }
    }
}

const handleAddClick = async (_) => {
    if (todo.value.trim()) {
        todo.classList.remove(INPUT_ERROR_CLASS)
        await createTodo(todo.value)
        todo.value = ''
        await renderTodos()
    } else {
        todo.classList.add(INPUT_ERROR_CLASS)
    }
}

const handleDeleteClick = async (ev) => {
    const todoId = ev.target.dataset['id']
    if (todoId) {
        await deleteTodo(todoId)
        await renderTodos()
    }
}

const renderTodos = async () => {
    const todos = await getTodo()
    const todosContainer = document.querySelector('#todo-list')
    todosContainer.innerHTML = ''

    todos.forEach(({ id, name }) => {
        const todo = strToDom(`<aside class="todo__item" data-id="${id}">
        
    </aside>`)

        const updateButton = strToDom(`<button type="button" class="btn todo__update" data-id="${id}">Update</button>`)
        const deleteButton = strToDom(`<button type="button" class="btn btn--error todo__delete" data-id="${id}">Delete</button>`)
        const input = strToDom(`<input type="text" name="todo-${id}" id="todo-${id}" value="${name}" data-id="${id}" />`)

        input.addEventListener('keydown', (ev) => {
            if (ev.key === 'Enter') {
                handleUpdateClick(ev)
            }
        })
        updateButton.addEventListener('click', handleUpdateClick)
        deleteButton.addEventListener('click', handleDeleteClick)

        todo.appendChild(input)
        todo.appendChild(updateButton)
        todo.appendChild(deleteButton)
        todosContainer.appendChild(todo)
    })

}

await main()