# Lab 5

Pour utiliser ce système, il faudra au préalable installer l’extension [ritwickdey.LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) sur Vscode ou le paquet node [http-server](https://www.npmjs.com/package/http-server).