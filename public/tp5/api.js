import { ref, onMounted } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'


export default function useApi() {
    const ENDPOINT = 'https://glo3102lab4.herokuapp.com'
    const headers = {
        "Content-Type": "application/json"
    }
    let userId = ref('')

    /**
     * We get user and fill userId variable.
     */
    const getUser = async () => {
        try {
            const res = await fetch(`${ENDPOINT}/users`, {
                method: 'POST',
                headers
            })
            userId.value = (await res.json()).id
        } catch (error) {
            console.log(`an error is raised : ${error}`);
        }
    }

    /**
     * This function get and returns todos of userId 
     * from API
     * @returns {Array} list of todos
     */
    const getTodo = async () => {
        if (userId.value) {
            try {
                const res = await fetch(`${ENDPOINT}/${userId.value}/tasks`, {
                    headers
                })
                return (await res.json()).tasks
            } catch (error) {
                console.log(`an error is raised : ${error}`);
            }
        }

    }

    /**
     * Function for create todo with todoName for the user
     * identified by userId variable (this must be not null).
     * @param {string} name name of todo
     */
    const createTodo = async (name) => {
        if (userId.value && name.trim()) {
            try {
                const res = await fetch(`${ENDPOINT}/${userId.value}/tasks`, {
                    method: 'POST',
                    headers,
                    body: JSON.stringify({
                        name
                    })
                })
                return (await res.json())
            } catch (error) {
                console.log(`an error is raised : ${error}`);
            }
        } else {
            console.log('userId is empty.');
        }
    }

    /**
     * Function for update todo for the user identified
     * by the userId variable
     * @param {string} id Id of todo to update
     * @param {string} name New value of todo
     */
    const updateTodo = async (id, name) => {
        if (userId.value && id && name.trim()) {
            try {
                const res = await fetch(`${ENDPOINT}/${userId.value}/tasks/${id}`, {
                    method: 'PUT',
                    headers,
                    body: JSON.stringify({
                        name,
                        id
                    })
                })
                return (await res.json())
            } catch (error) {
                console.log(`an error is raise : ${error}`)
            }
        }
    }

    /**
     * This function delete todo of userId from his id.
     * @param {string} id Id of todo to delete
     */
    const deleteTodo = async (id) => {
        if (userId && id) {

            try {
                const res = await fetch(`${ENDPOINT}/${userId.value}/tasks/${id}`, {
                    method: 'DELETE',
                    headers,
                    body: JSON.stringify({
                        id
                    })
                })
                return (await res.json())
            } catch (error) {

            }
        } else {
            console.log('UserId or todoId is empty.');
        }
    }

    onMounted(async () => {
        await getUser()
        await getTodo()
    })

    return {
        getUser,
        getTodo,
        createTodo,
        updateTodo,
        deleteTodo
    }
}
