import { createApp, reactive } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'
import useApi from './api.js'

createApp({
    setup() {
        const { getTodo, createTodo, updateTodo, deleteTodo } = useApi()
        const state = reactive({
            newTodo: '',
            todos: [],
            error: false
        })

        const addTodo = async () => {
            state.error = !await createTodo(state.newTodo)
            console.log(state.error);
            state.newTodo = ''
            await getTodos()
        }

        const changeTodo = async (todo) => {
            await updateTodo(todo.id, todo.name)
            await getTodos()
        }

        const removeTodo = async (todoId) => {
            await deleteTodo(todoId)
            await getTodos()
        }

        const getTodos = async () => {
            state.todos = await getTodo()
        }

        return {
            state,
            addTodo,
            changeTodo,
            removeTodo
        }


    }
}).mount('#app')