import { Toast } from './toast.js'

const toastButtons = document.querySelectorAll('.btn-toast')
const toastInput = document.querySelector('.toast-input')
const toastContainer = document.querySelector('#toasts')

toastButtons.forEach((btn) => {
    btn.addEventListener('click', (event) => {
        if (toastInput.value.length !== 0) {
            const toast = new Toast(event.target.dataset.type, toastInput.value, toastContainer)
            toast.createToast()
        }
    })
})