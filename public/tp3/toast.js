export class Toast {
    static removeTime = 2000

    constructor(type, text, container) {
        this.type = type
        this.text = text
        this.container = container
    }

    static strToDom(str) {
        return document.createRange().createContextualFragment(str).firstChild
    }

    createToast() {
        const toast = Toast.strToDom(`<aside class="toast toast--${this.type}"><p>${this.text}</p></aside>`)

        this.container.appendChild(toast)

        setTimeout((() => {
            toast.remove()
        }), Toast.removeTime)
    }
}