function showDropdown() {
    const root = document.querySelector('#dropdown')
    const listItems = [
        'grains',
        'dry fruits',
        'worms',
        'peas',
        'grapes',
        'grasshoppers',
        'but gentle',
        'cucumbers',
        'dried apricots',
        'crickets',
        'thistles',
        'oats',
        'dandelions',
    ]

    function strToDom(str) {
        return document.createRange().createContextualFragment(str).firstChild
    }

    function createClearElement() {
        const clearElement = strToDom(`<button class="dropdown__clear">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" 
                    stroke="currentColor" class="w-6 h-6"><path stroke - linecap="round" stroke - linejoin="round" 
                    d="M6 18L18 6M6 6l12 12" /></svg></button>`)
        clearElement.addEventListener('click', () => {
            inputElement.value = ''
            search(inputElement.value)
        })

        return clearElement

    }

    function search(value) {
        const list = listItems.filter((item) => item.startsWith(value))


        if (list.length === 0) {
            renderOptions('No result found')
        } else {
            renderOptions(list)
        }
    }

    function toggleOptions() {
        if (!root.classList.contains('show')) {
            showOptions()
        } else {
            hideOptions()
        }
    }

    function showOptions() {
        optionsElement.style.display = "block"
        window.setTimeout(() => {
            root.classList.add('show')
        }, 100)
    }

    function hideOptions() {
        root.classList.remove('show')
        window.setTimeout(() => {
            optionsElement.style.display = "none"
        }, 300)
    }

    function createInputElement() {
        const inputElement = strToDom('<input type="text" placeholder="Search…">')

        inputElement.addEventListener('click', () => {
            toggleOptions()
        })

        inputElement.addEventListener('blur', () => {
            hideOptions()
        })

        inputElement.addEventListener('input', (e) => {
            showOptions()
            search(e.target.value)
        })

        inputElement.addEventListener('keyup', (e) => {
            if (e.key === 'Escape') {
                hideOptions()
            }
        })

        return inputElement
    }

    function renderOptions(items) {
        let optionsElement = document.querySelector('#options')

        if (optionsElement) {
            optionsElement.remove()
        }


        optionsElement = createOptionsElement(items)

        root.appendChild(optionsElement)


    }

    function createOptionsElement(items) {
        const optionsElement = strToDom('<div id="options" class="dropdown__options"></div>')

        if (typeof items !== 'string') {
            items.forEach(item => {
                const option = strToDom(`<div>${item}</div>`)
                optionsElement.appendChild(option)
                option.addEventListener('click', (e) => {
                    inputElement.value = item
                })
            })
        } else {
            optionsElement.textContent = items
        }

        return optionsElement
    }

    const containerElement = strToDom('<div class="dropdown__field"></div>')

    const inputElement = createInputElement()
    const clearElement = createClearElement()
    const optionsElement = createOptionsElement(listItems)

    containerElement.appendChild(inputElement)
    containerElement.appendChild(clearElement)

    root.appendChild(containerElement)
    root.appendChild(optionsElement)

}