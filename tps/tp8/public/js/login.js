async function login() {
    let username = document.querySelector('#username').value
    let password = document.querySelector('#password').value
    try {
        const response = await fetch('/login', {
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
            method: 'POST',
        })

        if (response.status === 200) {
            Cookies.set('squirrel_cookie', (await response.json()).token)
            window.location.replace('/')
        }
    } catch (error) {
        console.log('There is an error :', error)
    }
}

document.querySelector('#login-form').addEventListener('submit', (e) => {
    e.preventDefault()
    login()
})
