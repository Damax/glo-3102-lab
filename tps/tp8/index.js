import Express from 'express'
import Crypto from 'crypto'
import CookieParser from 'cookie-parser'

const PORT = 8080

const users = []
const tokens = []

const app = Express()
app.set('view engine', 'ejs')
app.use(Express.json())
app.use(Express.static('public'))
app.use(CookieParser())

app.use('/users', (req, res) => {
    const username = req.body.username
    const password = req.body.password
    if (username && password) {
        users.push({ username, password })
        res.sendStatus(201)
    } else {
        res.sendStatus(400)
    }
})

app.get('/login', (_, res) => {
    res.render('login')
})

app.post('/login', (req, res) => {
    console.log(req.body)
    const username = req.body.username
    const password = req.body.password
    if (username && password) {
        const user = users.find(
            (u) => u.username === username && u.password === password
        )

        if (user === undefined) {
            res.sendStatus(403)
        } else {
            const token = Crypto.randomUUID()
            tokens.push({ username, token })
            res.send({ token })
        }
    } else {
        res.sendStatus(400)
    }
})

app.get('/', (req, res) => {
    const token = req.cookies.squirrel_cookie
    console.log(tokens)
    console.log(token);
    console.log(users);


    if (token) {
        const user = tokens.find((t) => t.token === token)
        if (user) {
            res.render('home', { username: user.username })
        } else {
            res.redirect('/login')
        }
    } else {
        res.redirect('/login')
    }
})

app.listen(PORT, () => {
    console.log(`🚀🐿️ Server running at http://auth.localhost:${PORT}/`)
})
