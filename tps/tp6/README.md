# Lab 6

Pour lancer le projet, il faudra commencer par installer les dépendances à l’aide de la commande : `npm install`.

Pour lancer le projet, il faut utiliser la commande `npm run dev`, le projet se lance alors sur : [localhost:5173](https://localhost:5173/tp6).

## Dépendances

Ce projet utilise :

- [Vite](https://vitejs.dev/) (builder)
- [VueUse](https://vueuse.org) (bibliothèque pour la composition API)